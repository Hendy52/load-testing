package com.seleniumproj;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    @FindBy(name = "op") 
    private WebElement loginButton; 
    
    @FindBy(id = "edit-name") 
    private WebElement usernameField; 
    
    @FindBy(id = "edit-pass") 
    private WebElement passField;
    
    public void setUsername(String username) { 
        usernameField.sendKeys(username); 
    } 
    
    public void setPassword(String password) { 
        passField.sendKeys(password); 
    }
    
    public void login() { 
        this.setUsername("hend.a@seznam.cz"); 
        this.setPassword("AAA123123"); 
        this.loginButton.click(); 
    } 

}
