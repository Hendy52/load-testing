package com.seleniumproj;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class LoadTest01 {
    
  private WebDriver driver;
  
  public LoadTest01() { // konstruktor
      super();
  }
   

  @BeforeClass
  public void beforeClass() {
      driver = new FirefoxDriver();
  }

  @AfterClass
  public void afterClass() {
      driver.quit();
  }
  
  @Test 
  public void loginTest() { 
      driver.get("https://www.mujrozhlas.cz/user/login");
      
      LoginPage loginPage = new LoginPage(); 
      loginPage.login(); 
       
      String actualUrl = driver.getCurrentUrl();
      String expectedUrl = "https://www.mujrozhlas.cz/";
      
      Assert.assertEquals(actualUrl, expectedUrl);
      System.out.println("Assert passed");
  }
  
 
}
